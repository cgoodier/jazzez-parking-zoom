//
//  Data.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//

import UIKit

class Data {
    
    static func getData(completion: @escaping ([ModelData]) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
        
            var data: [ModelData] = []
            
            let info = "Permitted parking only zones"
           
            data.append(ModelData(name: "Instruction For Permitted Parking", info: info, image: #imageLiteral(resourceName: "Instructions For Visitor Parking")))
            data.append(ModelData(name: "Car Pool", info: info, image: #imageLiteral(resourceName: "CarPoolxy")))
            data.append(ModelData(name: "Car Pool Upper", info: info, image: #imageLiteral(resourceName: "Car Pool Upperxy")))
            data.append(ModelData(name: "Contractor", info: info, image: #imageLiteral(resourceName: "Contractorxy")))
            data.append(ModelData(name: "Corridor 5", info: info, image: #imageLiteral(resourceName: "Corridor 5xy")))
//            data.append(ModelData(name: "Distinguished Visitor", info: info, image: #imageLiteral(resourceName: "Distinguished Visitor")Distinguished Visitor))
            data.append(ModelData(name: "Handicap", info: info, image: #imageLiteral(resourceName: "Handicapxy")))
            data.append(ModelData(name: "Mall", info: info, image: #imageLiteral(resourceName: "Mallxy")))
            data.append(ModelData(name: "MC", info: info, image: #imageLiteral(resourceName: "MCxy")))
            data.append(ModelData(name: "North A", info: info, image: #imageLiteral(resourceName: "North Axy")))
            data.append(ModelData(name: "North C", info: info, image: #imageLiteral(resourceName: "North Cxy")))
            data.append(ModelData(name: "North C Upper", info: info, image: #imageLiteral(resourceName: "North C Upperxy")))
            data.append(ModelData(name: "North Secured", info: info, image: #imageLiteral(resourceName: "North Securedxy")))
            data.append(ModelData(name: "Official Business", info: info, image: #imageLiteral(resourceName: "Official Businessxy")))
            data.append(ModelData(name: "Pentagon Conference Center", info: info, image: #imageLiteral(resourceName: "Pentagon Conference Centerxy")))
            data.append(ModelData(name: "Press", info: info, image: #imageLiteral(resourceName: "Pressxy")))
            data.append(ModelData(name: "River", info: info, image: #imageLiteral(resourceName: "Riverxy")))
            data.append(ModelData(name: "South A", info: info, image: #imageLiteral(resourceName: "South Axy")))
            data.append(ModelData(name: "South A Secured", info: info, image: #imageLiteral(resourceName: "South A Securedxy")))
            data.append(ModelData(name: "South C 1-5", info: info, image:   #imageLiteral(resourceName: "South C 1-5xy")))
            data.append(ModelData(name: "South C 20-25", info: info, image:  #imageLiteral(resourceName: "South C 20-25xy")))
            data.append(ModelData(name: "South C 27-35", info: info, image:  #imageLiteral(resourceName: "South C 27-35xy")))
            data.append(ModelData(name: "South C Eads St", info: info, image:  #imageLiteral(resourceName: "South C Eads Stx")))
            data.append(ModelData(name: "South C Fern St", info: info, image:  #imageLiteral(resourceName: "South C Fern Stx")))
            data.append(ModelData(name: "South C Hayes St", info: info, image:  #imageLiteral(resourceName: "South C Hayes Stxy")))
            data.append(ModelData(name: "Tri-Care", info: info, image: #imageLiteral(resourceName: "Tri-Carex")))
            data.append(ModelData(name: "VIP", info: info, image: #imageLiteral(resourceName: "VIPxy")))
            data.append(ModelData(name: "VIP Upper", info: info, image: #imageLiteral(resourceName: "VIP Upperxy")))
            
//           sleep(2)
            
            DispatchQueue.main.async {
                completion(data)
            }
        }
    }
    
    //    func completion(data: [ModelData]) {
    //
        }
//}























