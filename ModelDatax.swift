//
//  ModelData.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//

import UIKit

class ModelDatax {
    var namex = ""
    var infox = ""
    var imagex: UIImage?
    
    init(namex: String, infox: String, imagex: UIImage?) {
        self.namex = namex
        self.infox = infox
        self.imagex = imagex
    }
}
