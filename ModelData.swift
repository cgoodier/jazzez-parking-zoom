//
//  ModelData.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//

import UIKit

class ModelData {
    var name = ""
    var info = ""
    var image: UIImage?
    
    init(name: String, info: String, image: UIImage?) {
        self.name = name
        self.info = info
        self.image = image
    }
}
