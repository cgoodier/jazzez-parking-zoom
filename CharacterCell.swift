//
//  CharacterCell.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//

import UIKit

class CharacterCell: UITableViewCell {

    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    func setup(item: ModelData) {
        name.text = item.name
        desc.text = item.info
        characterImage.image = item.image
    }
}
