//
//  CharacterCell.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//

import UIKit

class CharacterCellx: UITableViewCell {

    @IBOutlet weak var characterImagex: UIImageView!
    @IBOutlet weak var namex: UILabel!
    @IBOutlet weak var descx: UILabel!
    
    func setupx(itemx: ModelDatax) {
        namex.text = itemx.namex
        descx.text = itemx.infox
        characterImagex.image = itemx.imagex
    }
}
